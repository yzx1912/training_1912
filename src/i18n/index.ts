import en from "./config/en"
import zh from "./config/zh"
import { createI18n } from "vue-i18n"

const messages = {
  zh,
  en,
}

const i18n = createI18n({
  locale: "zh",
  fallbackLocale: "en",
  legacy: false,
  messages,
})

export default i18n
