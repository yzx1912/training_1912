const config = {
  文章: "article",
  归档: "archive",
  知识小册: "kbrochure",
  推荐阅读: "A Wrinkle in Time",
  文章标签: "The article label",
  文章分类: "The article classification",
}

export default config
