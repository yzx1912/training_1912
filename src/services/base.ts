import http from "@/utils/httpTool"
import { Params } from "@/utils/httpTool"

// 动态路由
export const getNavPage = (params: Params = {}) => http.get("/api/page", params)
// 文章列表
export const getList = () => http.get("/api/article/recommend")
// 文章标签
export const getTag = () =>
  http.get("/api/tag", {
    articleStatus: "publish",
  })
// 知识小册
export const getViews = (params: Params) => http.get("/api/knowledge", params)
//Wwwww
// export const getWwwww = (params: Params) => http.get("/api/page", 1,12)
// 文章分类
export const getClass = () => http.get("/api/category")
export const getCategory = (category: string, params: Params) => {
  params = category === "all" ? params : { ...params, category }
  return http.get("/api/article", params)
}




export const GetDetailData = (id:string) => http.post("/api/article/"+id+"/views")
