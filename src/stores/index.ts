import { defineStore } from "pinia"
import { base } from "@/services"
import { Params } from "@/utils/httpTool"

const useStore = defineStore<string, any>("baseStore", {
  state: () => ({
    navList: [], // 路由
    list: [], // 文章列表
    tag: [], // 文章标签
    skList: [], // 知识小册
    classList: [], // 文章分类
    categoryList: [[], 0], // 文章页面的分类
    detailList: [], //分类详情
    EditDeilList: [], //知识小册二级详情
    WwwwList: [], //四个WWWW
    searchList: [], // 搜索的数据
    detailListDetil: [], //知识小册李的详情里的详情
    toc:[]
  }),
  actions: {
    // 路由
    async getNavData() {
      const { data } = await base.getNavPage()
      this.$patch({
        navList: data[0]
          .filter((item: any) => item.id)
          .sort((a: any, b: any) => b.order - a.order),
      })
    },
    // 文章页面数据
    async getListAction() {
      const { data } = await base.getList()
      this.$patch({
        list: data,
      })
      return data
    },
    // 知识小册
    async getListViews() {
      const { data } = await base.getViews({})
      this.$patch({
        skList: data[0],
      })
      return data[0]
    },
    // 文章标签
    async getTagAction() {
      const { data } = await base.getTag()
      this.$patch({
        tag: data,
      })
      return data
    },
    // 文章分类
    async getClassList() {
      const { data } = await base.getClass()
      this.$patch({
        classList: data,
      })
      return data
    },
    clearArticleCategoryList() {
      this.$patch({
        categoryList: [[], 0],
      })
    },

    async getCategory(type: string, params: Params) {
      const { data } = await base.getCategory(type, params)
      this.$patch({
        categoryList: [[...this.categoryList[0], ...data[0]], data[1]],
      })
    },
    //我是四个WWWW
    // async getWWWW(){
    // const { data } = await base.getWwwww()
    // this.$patch({
    // WwwwList: data
    // })
    // return data
    // },
    //分类详情方法
    async getEditList(id?: any) {
      this.detailList = this.list.filter((value: any) => {
        return value.id === id
      })
    },
    //知识小册的详情的详情方法
    async getEditDeil(id: any) {
      this.detailListDetil = this.skList.filter((value: any) => {
        return value.id === id
      })
    },
    // 搜索
    async searchClick(searchValue: string) {
      this.searchList = this.list.filter((item: any) => {
        return item.title.includes(searchValue)
      })
    },
    async getToc(id:string){
      const { data } =await base.GetDetailData(id)
      this.$patch({
        toc:  JSON.parse(data.toc)  
      })
    }
  },
})
export default useStore
