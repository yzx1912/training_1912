const baseRouter = [
  {
    path: "/article",
    name: "article",
    component: () => import("@/views/Article.vue"),
    redirect: "/category/all",
    children: [
      {
        path: "/category/:id",
        component: () => import("@/views/category/index.vue"),
        meta: {
          title: "分类",
        },
      },
    ],
    meta: {
      title: "文章",
      nav: true,
    },
  },
  {
    path: "/archive",
    name: "archive",
    component: () =>
      import(/* webpackChunkName: "archive" */ "@/views/Archive.vue"),
    meta: {
      title: "归档",
      nav: true,
    },
  },
  {
    path: "/kbrochure",
    name: "kbrochure",
    component: () =>
      import(/* webpackChunkName: "kbrochure" */ "@/views/Kbrochure.vue"),
    meta: {
      title: "知识小册",
      nav: true,
    },
  },
  {
    path: "/detail/:id",
    name: "detail",
    component: () =>
      import(/* webpackChunkName: "detail" */ "@/views/Detail.vue"),
    meta: {
      title: "详情",
      nav: false,
    },
  },
  {
    path: "/page/:id",
    component: () => import("@/views/page/index.vue"),
    meta: {
      nav: false,
    },
  },
  {
    path: "/archiveDetail/:id",
    component: () => import("@/views/ArchiveDetail.vue"),
    meta: {
      nav: false,
    },
  },
  {
    path: "/KbrochureDetil/:id",
    component: () => import("@/views/KbrochureDetil.vue"),
    meta: {
      nav: false,
    },
  },
  {
    path: "/",
    redirect: "/article",
  },
  {
    path:'/:pathMatch(.*)',
    alias:"/404",
    component:()=>import('../views/404/index.vue'),
    meta: {
      nav: false,
      title:'页面找不到',
    },
  }
]

export default baseRouter
