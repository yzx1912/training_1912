import BaseHeader from "@/components/baseHeader/index.vue"
import BaseList from "@/components/baseList/index.vue"
import Particulars from "@/components/particulars/index.vue"
import BaseSearch from "@/components/baseSearch/index.vue"
import Comment from "@/components/comment/index.vue"
import BackTop from "@/components/backtop/index.vue"
import BaseShare from "@/components/baseShare/index.vue"
import BaseModal from "@/components/baseModal/index.vue"
import BaseScroll from "@/components/baseScroll/index.vue"
import BaseFloor from "@/components/floor/index.vue"
import { App } from "vue"
export default {
  install(app: App): void {
    app.component("base-header", BaseHeader)
    app.component("base-list", BaseList)
    app.component("base-Particulars", Particulars)
    app.component("base-search", BaseSearch)
    app.component("base-BackTop", BackTop)
    app.component("base-share", BaseShare)
    app.component("base-Comment", Comment)
    app.component("base-modal", BaseModal)
    app.component("base-scroll", BaseScroll)
    app.component("base-floor", BaseFloor)
  },
}
