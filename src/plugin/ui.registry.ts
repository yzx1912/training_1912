import {
  Button,
  message,
  ConfigProvider,
  Card,
  Input,
  Checkbox,
  BackTop,
  Anchor,
  Carousel,
  Comment,
  List,
  Modal,
  Popover,
} from "ant-design-vue"
import { App } from "vue"
const install = (app: App) => {
  app.use(Button)
  app.use(Card)
  app.use(ConfigProvider)
  app.use(Input)
  app.use(Checkbox)
  app.use(BackTop)
  app.use(Anchor)
  app.use(Carousel)
  app.use(Comment)
  app.use(List)
  app.use(Modal)
  app.use(Popover)
  app.config.globalProperties.$message = message
}
export default {
  install,
}
